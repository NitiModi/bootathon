var num1 : HTMLInputElement = <HTMLInputElement>(document.getElementsByName('num1')[0]);
var num2 : HTMLInputElement = <HTMLInputElement>(document.getElementsByName('num2')[0]);
var ans : HTMLInputElement = <HTMLInputElement>document.getElementById('ans');
var op = <NodeListOf<HTMLInputElement>>(document.getElementsByName('op'));

function po(){
	var x;
	for(var i in op) {
		if(op[i].checked){
			x = op[i];
			break;
		}
	}
	if((x.value).toString() == "add"){
		var out : number = parseFloat(num1.value) + parseFloat(num2.value);
		ans.value = out.toString();
	}
	else if((x.value).toString() == "sub"){
		var out : number = parseFloat(num1.value) - parseFloat(num2.value);
		ans.value = out.toString();
	}
	else if((x.value).toString() == "mul"){
		var out : number = parseFloat(num1.value) * parseFloat(num2.value);
		ans.value = out.toString();
	}
	else if((x.value).toString() == "div"){
		if(parseFloat(num2.value) == 0){
			ans.value = "Wrong Input";
		}
		else{
			var out : number = parseFloat(num1.value) / parseFloat(num2.value);
			ans.value = out.toString();
		}
	}
}