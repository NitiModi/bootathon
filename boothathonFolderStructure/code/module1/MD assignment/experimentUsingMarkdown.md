# **EXPERIMENT:&nbsp;<b>Acid Base Titration**

## **AIM:**
>To determine the amount of substance in a solution of unknown concentration using various titrimetric methods.


## **THEORY:**
>The word titration comes from the Latin word "titulus", which means inscription or title. The French word title means rank. Therefore, Titration means the determination of concentration or rank of a solution with respect to water with a pH of 7. 

>The standard solution is usually added from a graduated vessel called a burette. The process of adding standard solution until the reaction is just complete is termed as titration and the substance to be determined is said to be titrated.

>All chemical reactions cannot be considered as titrations. A reaction can serve as a basis of a titration procedure only if the following conditions are satisfied:

1. The reaction must be a fast one.
2. It must proceed stoichiometrically.
3. The change in free energy (ΔG) during the reaction must be sufficiently large for spontaneity of the reaction.
4. There should be a way to detect the completion of the reaction.
 
 ### **End point and Equivalent point:**

 >For a reaction, a stage which shows the completion of a particular reaction is known as end point. Equivalence point is a stage in which the amount of reagent added is exactly and stoichiometrically equivalent to the amount of the reacting substance in the titrated solution. The end point is detected by some physical change produced by the solution, by itself or more usually by the addition of an auxiliary reagent known as an 'indicator'. The end point and the equivalence point may not be identical. End point is usually detected only after adding a slight excess of the titrant. In many cases, the difference between these two will fall within the experimental error.
 
 ### **Indicator:**
 >It is a chemical reagent used to recognize the attainment of end point in a titration. After the reaction between the substance and the standard solution is complete, the indicator should give a clear colour change.

 >When a titration is carried out, the free energy change for the reaction is always negative.
  That is, during the initial stages of the reaction between A & B, when the titrant A is added to B the following reaction takes place.

 >![equation1](img1.jpg) 

>Equilibrium constant,     
>  ![equation2](img2.jpg) 

>here,    a  = activity co-efficient.

>Large values of the equilibrium constant K implies that the equilibrium concentration of A & B are very small at the equivalence point. It also indicates that the reverse reaction is negligible and the product C & D are very much more stable than the reactants A & B. Greater the value of K the larger the magnitude of the negative free energy change for the reaction between A & B. Since,

 >![equation3](img3.jpg) 

> Where,

>R = Universal gas Constant = 8.314 JK-1mol-1,
>T = Absolute Temperature.

>The reaction of the concentration of A & B leads to the reduction of the total free energy change. If the concentrations of A & B are too low the magnitude of the total free energy change becomes so small and the use of the reaction for titration will not be feasible.

 ## **Expressions of Concentration of Solutions:**
 >The concentration or strength of solution means the amount of solute present in a given amount of the solution. The concentration may be expressed in physical or chemical units.

 1. ***Normality*** (N): It is defined as number of gram equivalents of the solute present in 1 litre (1000mL.) of the solution. If W g of solute of equivalent weight E is present in V mL of the solution, the normality of the solution is given by:

 >![equation4](img4.png) 

2. ***Molarity*** (M): It is defined as the number of moles of the solute present in 1 litre (or 1000 mL) of the solution. A one molar solution contains 1 mole of the solute dissolved in 1 litre of the solution.
 

3. ***Molality*** (m): It is defined as the number of moles of solute dissolved in 1000 g of the solvent. One molal solution contains one mole of the solute dissolved in 1000 g of the solvent.

## **Normal solution:**

>A solution containing one gram equivalent weight of the solute dissolved per litre is called a normal solution; e.g. when 40 g of NaOH are present in one litre of NaOH solution, the solution is known as normal (N) solution of NaOH. Similarly, a solution containing a fraction of gram equivalent weight of the solute dissolved per litre is known as subnormal solution. For example, a solution of NaOH containing 20 g (1/2 of g eq. wt.) of NaOH dissolved per litre is a sub-normal solution. It is written as N/2 or 0.5 N solution.

 >Formulae used in solving numerical problems on volumetric analysis.

 Strength of solution = Amount of substance in g litre-1.
 

1. Strength of solution = Amount of substance in g moles litre-1.
 

2. Strength of solution = Normality × Eq. wt. of the solute = molarity × Mol. wt. of solute.
 

3. Molarity = Moles of solute/Volume in litre.
 

4. Number of moles = Wt.in g/Mol. wt = M × V (initial) = Volume in litres/22.4 at NTP (only for gases).
 

5. Number of milli moles = Wt. in g × 1000/mol. wt. = Molarity × Volume in mL.
 

6. Number of equivalents= Wt. in g/Eq. wt = x × No. of moles × Normality × Volume in litre (Where x = Mol. wt/Eq. wt).
 

7. Number of mill equivalents (meq.) = Wt. in g × 1000 / Eq. wt = normality × volume in mL.
 

8. Normality = x × No. of mill moles (Where x = valency or change in oxi. number).
 

9. Normality formula, N1V1 = N2V2, (Where N1, N2 → Normality of titrant and titrate respectively, V1, V2 → Volume of titrant and titrate respectively).
 

10. % by weight = Wt. of solvent/Wt. of solution × 100 .
## **Volumetric Analysis:**

It involves the estimation of a substance in solution by neutralization, precipitation, oxidation or reduction by means of another solution of accurately known strength. This solution is known as standard solution.

 

Volumetric analysis depends on measurements of the volumes of solutions of the interacting substances. A measured volume of the solution of a substance A is allowed to react completely with the solution of definite strength of another substance B. The volume of B is noted. Thus we know the volume of the solutions A and B used in the reaction and the strength of solution B; so the strength of the other solution A is obtained. The amount (or concentration) of the dissolved substance in volumetric analysis is usually expressed in terms of normality. The weight in grams of the substance per litre of the solution is related to normality of the solution as,

>![equation5](img5.png) 

### **Conditions of Volumetric Analysis:**
 

>i) The reaction between the titrant and titrate must be expressed.

>ii) The reaction should be practically instantaneous.

>iii) There must be a marked change in some physical or chemical property of the solution at the end point.

>iv) An indicator should be available which should sharply define the end point.

 
## **Different methods to determine the endpoint include:**
 

*pH indicator:*
 

A pH indicator is a substance that it changes its colour in response to a chemical change. An acid-base indicator changes its colour depending on the pH (e.g., phenolphthalein). Redox indicators are also frequently used. A drop of indicator solution is added to the titration at the start; at the endpoint has been reached the colour changes.

 

*A potentiometer*
 

It is an instrument that measures the electrode potential of the solution. These are used for titrations based on a redox reaction; the potential of the working electrode will suddenly change as the endpoint is reached.

 

*pH meter:*
 

It is a potentiometer that uses an electrode whose potential depends on the amount of H+ ion present in the solution. (It is an example of an ion-selective electrode.) This allows the pH of the solution to be measured throughout the titration. At the endpoint, there will be a sudden change in the measured pH. This method is more accurate than the indicator method and is very easily automated.

*Conductance:*
 

The conductivity of a solution depends on the ions present in it. During many titrations, the conductivity changes significantly. (i.e., during an acid-base titration, the H+ and OH- ions react to form neutral H2O, this changes the conductivity of the solution.) The total conductance of the solution also depends on the other ions present in the solution, such as counter ions. This also depends on the mobility of each ion and on the total concentration of ions that is the ionic strength.

 

*Colour change:*
 

In some reactions, the solution changes colour without any added indicator. This is often seen in redox titrations, for instance, when the different oxidation states of the product and reactant produce different colours.

 

*Precipitation:*
 

In this type of titration the strength of a solution is determined by its complete precipitation with a standard solution of another substance.

![equation6](img6.gif) 

### **Acid base titration:**
 

>The chemical reaction involved in acid-base titration is known as neutralisation reaction. It involves the combination of H3O+ ions with OH- ions to form water. In acid-base titrations, solutions of alkali are titrated against standard acid solutions. The estimation of an alkali solution using a standard acid solution is called acidimetry. Similarly, the estimation of an acid solution using a standard alkali solution is called alkalimetry.

## **The Theory of Acid–Base Indicators: **
 

>Ostwald, developed a theory of acid base indicators which gives an explanation for the colour change with change in pH. According to this theory, a hydrogen ion indicator is a weak organic acid or base. The undissociated molecule will have one colour and the ion formed by its dissociation will have a different colour.

>Let the indicator be a weak organic acid of formulae HIn. It has dissociated into H+ and In- . The unionized molecule has one colour, say colour (1), while the ion, In- has a different colour, say colour (2). Since HIn and In- have different colours, the actual colour of the indicator will dependent upon the hydrogen ion concentration [H+]. When the solution is acidic, that is the H+ ions present in excess, the indicator will show predominantly colour (1). On other hand, when the solution is alkaline, that is, when OH- ions present in excess, the H+ ions furnished by the indicator will be taken out to form undissociated water. Therefore there will be larger concentration of the ions, In-. thus the indicator will show predominantly colour (2).  

>Some indicators can be used to determine pH because of their colour changes somewhere along the change in pH range. Some common indicators and their respective colour changes are given below.

| INDICATORS  | COLOR ON ACIDIC SIDE | RANGE OF COLORS | 
|---------------|:------------------:|---------------:|
| Methyl Violet |  Yellow            | 0.0 - 1.6 |
| Bromophenol Blue |    Yellow       | 3.0 - 4.6 |
| Methyl Orange    | Red             | 3.1 - 4.4 |

i.e., at pH value below 5, litmus is red; above 8 it is blue. Between these values, it is a mixture of two colours.


# **Procedure:**
> - Choose the titrant.
> - Choose the titrate.
> - Select the normality of the titrate.
> - Choose the volume of the liquid to be pipetted out.
> - Select the indicator.
> - Start titration.
> - End point is noted at the colour change of the solution.
> - From the final reading the normality of titrant can be calculated by the equation:Choose the titrant.
Choose the titrate.
> - Select the normality of the titrate.
> - Choose the volume of the liquid to be pipetted out.
Select the indicator.
> - Start titration.
> - End point is noted at the colour change of the solution.
> - From the final reading the normality of titrant can be calculated by the equation: